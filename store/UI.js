export const state = () => ({
    addPlaceStepNumber: 1
  });
  
  export const mutations = {
    setAddPlaceStepNumber (state, step) {
        console.log('set step')
        state.addPlaceStepNumber = step
    }
  };
  
  export const actions = {
  };
  
  export const getters = {
    addPlaceStepNumber (state) {
        return state.addPlaceStepNumber
    }
  };