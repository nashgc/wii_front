export const state = () => ({
  map_center: [59.9051232680777, 30.312995910644535],
  markerLatLng: { lat: "", lng: "" },
  placeDetailDescription: "",
  placeDetailPhotos: [],
  placeAverageRating: 0,
  currentPlaceId: null
});

export const mutations = {
  set_map_center(state, coords) {
    state.map_center = coords;
  },
  set_marker_lat_lng(state, coords) {
    if (Array.isArray(coords)) {
      state.markerLatLng.lat = coords[0];
      state.markerLatLng.lng = coords[1];
    } else {
      state.markerLatLng.lat = coords.lat;
      state.markerLatLng.lng = coords.lng;
    }
  },
  setPlaceDetailData(state, { description, average_rating, photos }) {
    console.log("seeet", description, average_rating);
    state.placeDetailDescription = description;
    state.placeAverageRating = average_rating;
    photos.forEach((photo) => {
      state.placeDetailPhotos.push(photo)
    })
  },
  setCurrentPlaceId(state, placeId) {
    state.currentPlaceId = placeId
  }
};

export const actions = {};

export const getters = {
  getCurrentPlaceId (state) {
    return state.currentPlaceId
}
};
