FROM node:14

WORKDIR /home/whereisit

COPY . .
RUN npm install
RUN npm run build

EXPOSE 3000

ENV HOST=0.0.0.0
ENV PORT=3000